package com.taobao.fanli.controller;

import com.taobao.fanli.common.RestResponse;
import com.taobao.fanli.dao.model.Link;
import com.taobao.fanli.dao.model.LinkSource;
import com.taobao.fanli.service.LinkService;
import com.taobao.fanli.service.LinkSourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/link")
@Api(value="短地址",description="短地址接口")
public class LinkController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private LinkService linkService;
    @Resource
    private LinkSourceService linkSourceService;

    @RequestMapping(value = "/jump" , method = RequestMethod.GET)
    @ApiOperation(value="jump", notes="jump", httpMethod = "GET")
    public void jump(
            HttpServletResponse response,
            @RequestParam(value = "id") Integer id) throws IOException {
        Link link = linkService.selectByPrimaryKey(id);
        response.sendRedirect(link.getSource());
    }

    @RequestMapping(value = "/find" , method = RequestMethod.GET)
    @ApiOperation(value="find", notes="find", httpMethod = "GET")
    @ResponseBody
    public RestResponse find(
            @RequestParam(value = "id") Integer id){
        Link link = linkService.selectByPrimaryKey(id);
        if (null == link){
            return new RestResponse("连接不存在");
        }
        return new RestResponse(link);
    }

    @RequestMapping(value = "/add" , method = RequestMethod.POST)
    @ApiOperation(value="add", notes="add", httpMethod = "POST")
    @ResponseBody
    public RestResponse add(
            @RequestParam(value = "type", defaultValue = "1") Byte type,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "source") String source){
        Link link = new Link();
        link.setType(type);
        link.setName(name);
        link.setSource(source);
        link.setCreateAt(new Date());
        linkService.insertSelective(link);
        return new RestResponse("添加成功");
    }

    @RequestMapping(value = "/update" , method = RequestMethod.POST)
    @ApiOperation(value="update", notes="update", httpMethod = "POST")
    @ResponseBody
    public RestResponse update(
            @RequestParam(value = "id") Integer id,
            @RequestParam(value = "type", defaultValue = "1") Byte type,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "source") String source){
        Link link = new Link();
        link.setId(id);
        link.setType(type);
        link.setName(name);
        link.setSource(source);
        linkService.updateByPrimaryKeySelective(link);
        return new RestResponse("更新成功");
    }

    @RequestMapping(value = "/delete" , method = RequestMethod.POST)
    @ApiOperation(value="delete", notes="delete", httpMethod = "POST")
    @ResponseBody
    public RestResponse delete(
            @RequestParam(value = "id") Integer id){
        linkService.deleteByPrimaryKey(id);
        return new RestResponse("删除成功");
    }

    @RequestMapping(value = "/updateBySource")
    @ApiOperation(value="updateBySource", notes="updateBySource", httpMethod = "GET")
    @ResponseBody
    public RestResponse updateBySource(
            @RequestParam(value = "id") Integer id,
            @RequestParam(value = "sourceId") Integer sourceId){
        int i = linkService.updateBySourceId(id, sourceId);
        if (i < 1){
            return new RestResponse("更新失败");
        }
        return new RestResponse("更新成功");
    }

    @RequestMapping(value = "/source/list/web" , method = RequestMethod.GET)
    public String sourceListWeb(@RequestParam(value = "linkId") Integer linkId,
                       HashMap<String, Object> map){
        List<LinkSource> linkSources = linkSourceService.selectByLinkId(linkId);

        map.put("linkSources", linkSources);
        map.put("linkId", linkId);
        return "link/sourceList";
    }

    @RequestMapping(value = "/source/add/web" , method = RequestMethod.GET)
    public String sourceAddWeb(
            @RequestParam(value = "linkId") Integer linkId,
            HashMap<String, Object> map
    ){
        map.put("linkId", linkId);
        return "link/sourceAdd";
    }

    @RequestMapping(value = "/source/list" , method = RequestMethod.GET)
    @ApiOperation(value="/source/list", notes="add", httpMethod = "POST")
    @ResponseBody
    public RestResponse sourceList(
            @RequestParam(value = "linkId") Integer linkId){
        List<LinkSource> linkSources = linkSourceService.selectByLinkId(linkId);
        return new RestResponse(linkSources);
    }

    @RequestMapping(value = "/source/info" , method = RequestMethod.GET)
    @ApiOperation(value="/source/info", notes="add", httpMethod = "POST")
    @ResponseBody
    public RestResponse sourceAdd(
            @RequestParam(value = "id") Integer id){
        LinkSource linkSource = linkSourceService.selectByPrimaryKey(id);
        return new RestResponse(linkSource);
    }

    @RequestMapping(value = "/source/add" , method = RequestMethod.POST)
    @ApiOperation(value="/source/add", notes="add", httpMethod = "POST")
    @ResponseBody
    public RestResponse sourceAdd(
            @RequestParam(value = "linkId") Integer linkId,
            @RequestParam(value = "source") String source){
        LinkSource linkSource = new LinkSource();
        linkSource.setLinkId(linkId);
        linkSource.setSource(source);
        linkSource.setMd5(DigestUtils.md5Hex(source));
        linkSourceService.insertSelective(linkSource);
        return new RestResponse("添加成功");
    }

    @RequestMapping(value = "/source/update" , method = RequestMethod.POST)
    @ApiOperation(value="/source/update", notes="update", httpMethod = "POST")
    @ResponseBody
    public RestResponse sourceUpdate(
            @RequestParam(value = "id") Integer id,
            @RequestParam(value = "linkId") Integer linkId,
            @RequestParam(value = "source") String source){
        LinkSource linkSource = new LinkSource();
        linkSource.setId(id);
        linkSource.setLinkId(linkId);
        linkSource.setSource(source);
        linkSource.setMd5(DigestUtils.md5Hex(source));
        linkSourceService.updateByPrimaryKeySelective(linkSource);
        return new RestResponse("更新成功");
    }


    @RequestMapping(value = "/source/delete" , method = RequestMethod.POST)
    @ApiOperation(value="/source/delete", notes="/source/delete", httpMethod = "POST")
    @ResponseBody
    public RestResponse sourceDelete(
            @RequestParam(value = "id") Integer id){
        linkSourceService.deleteByPrimaryKey(id);
        return new RestResponse("更新成功");
    }


}
