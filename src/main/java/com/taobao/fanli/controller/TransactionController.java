package com.taobao.fanli.controller;

import com.taobao.fanli.common.RestResponse;
import com.taobao.fanli.service.LoginService;
import com.taobao.fanli.service.TransactionService;
import com.taobao.fanli.vo.Login;
import com.taobao.fanli.vo.TransactionListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
@Api(value="账单接口api", description = "账单接口api")
@RestController
@RequestMapping(value = "/transaction")
public class TransactionController {

    private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Resource
    private LoginService loginService;
    @Resource
    private TransactionService transactionService;


    @RequestMapping(value = "list", method = RequestMethod.GET)
    @ApiOperation(value="账单列表", notes="账单列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "账单状态 1:冻结金额 2:可提取金额 3:已提取金额",  required = false, dataType = "Byte", paramType = "query"),
            @ApiImplicitParam(name = "from", value = "账单来源 1:红包 2:订单 3:用户", required = false, dataType = "Byte", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "page", required = true, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "size", required = false, dataType = "Integer", paramType = "query")
    })
    public RestResponse transactionList(
            @RequestParam(value = "type", required = false) Byte type,
            @RequestParam(value = "from", required = false) Byte from,
            @RequestParam(value = "page") Integer page,
            @RequestParam(value = "size", required = false , defaultValue = "10") Integer size,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Login userByCookie = loginService.getUserByCookie(request);
        if(userByCookie == null){
            return new RestResponse("用户没有登录", 305);
        }
        try {
            TransactionListVo transactionListVo = transactionService.list(userByCookie.getId(), type, from, page, size);
            return new RestResponse(transactionListVo);
        } catch (Exception e) {
            logger.error("账单列表查询失败：" , e);
            return new RestResponse("账单列表查询失败", 505);
        }
    }
}
