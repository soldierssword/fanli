package com.taobao.fanli.controller;

import com.taobao.fanli.common.RestResponse;
import com.taobao.fanli.dao.model.UserTransaction;
import com.taobao.fanli.postbean.UpdateUserTransactionBean;
import com.taobao.fanli.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * @author:xiaolei
 * @date:2018/6/17
 */
@RestController
@RequestMapping("/admin")
@Api(description = "后台管理")
public class AdminController {

    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Resource
    private AdminService adminService;

    @RequestMapping(value = "updateUserTransaction", method = RequestMethod.POST)
    @ApiOperation(value="更新用户提现接口", notes="更新用户提现接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "userTransaction主键", required = true, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "state", value = "状态", required = true, dataType = "Integer", paramType = "query")
    })
    public RestResponse updateUserTransaction(
        @Valid UpdateUserTransactionBean updateUserTransactionBean, BindingResult result
    ){
        if(result.hasErrors()){
            List<ObjectError> allErrors = result.getAllErrors();
            for (ObjectError allError : allErrors){
                return new RestResponse(allError.getDefaultMessage(), 505);
            }
        }
        try {
            adminService.updateUserTransaction(updateUserTransactionBean);
            return new RestResponse("success");
        } catch (RuntimeException e) {
            logger.error("更新用户提现失败:", e);
            return new RestResponse(e.getMessage(), 505);
        }
    }

}
