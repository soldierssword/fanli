package com.taobao.fanli.controller;

import com.taobao.fanli.common.RestResponse;
import com.taobao.fanli.dao.model.ReceiveRedpack;
import com.taobao.fanli.postbean.RedpackInitBean;
import com.taobao.fanli.service.LoginService;
import com.taobao.fanli.service.RedPackService;
import com.taobao.fanli.vo.Login;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/redpack")
@Api(value="红包接口api", description = "红包接口api")
public class RedPackController {

    private static final Logger logger = LoggerFactory.getLogger(RedPackController.class);

    @Resource
    private RedPackService redPackService;
    @Resource
    private LoginService loginService;

    @RequestMapping(value = "/init", method = RequestMethod.POST)
    @ApiOperation(value="红包初始化接口", notes="红包初始化接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType =
                    "String", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "类型", required = true, dataType =
                    "String", paramType = "query")
    })
    public RestResponse init(
            @Valid RedpackInitBean redpackInitBean, BindingResult result
    ){
            if(result.hasErrors()){
                List<ObjectError> allErrors = result.getAllErrors();
                for (ObjectError allError : allErrors){
                    return new RestResponse(allError.getDefaultMessage(), 505);
                }
            }
            try{
                redPackService.init(redpackInitBean);
                return new RestResponse("success");
            }catch (RuntimeException e){
                return new RestResponse(e.getMessage(), 505);
            }
    }

    @ApiOperation(value="可领取的红包接口", notes="可领取的红包接口", httpMethod = "GET")
    @RequestMapping(value = "/queryList", method = RequestMethod.GET)
    public RestResponse queryList(
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Login userByCookie = loginService.getUserByCookie(request);
        if(userByCookie == null){
            return new RestResponse("用户没有登录", 305);
        }
        try{
            List<ReceiveRedpack> receiveRedpacks = redPackService.queryList(userByCookie);
            return new RestResponse(receiveRedpacks);
        }catch (Exception e){
            logger.error("可领取的红包接口查询失败:", e);
            return new RestResponse("可领取的红包接口查询失败", 505);
        }
    }

    @ApiOperation(value="更新可领取的红包接口", notes="更新可领取的红包接口", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "redpackId", value = "红包id", required = true, dataType =
                    "String", paramType = "query")
    })
    @RequestMapping(value = "/updateReceiveRedpack", method = RequestMethod.POST)
    public RestResponse updateReceiveRedpack(
            @RequestParam(value = "redpackId") Integer redpackId,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Login userByCookie = loginService.getUserByCookie(request);
        if(userByCookie == null){
            return new RestResponse("用户没有登录", 305);
        }
        try{
            redPackService.updateReceiveRedpack(userByCookie, redpackId);
            return new RestResponse("success");
        }catch (RuntimeException e){
            return new RestResponse(e.getMessage(), 505);
        } catch (Exception e){
            logger.error("更新可领取的红包失败:", e);
            return new RestResponse("更新可领取的红包失败", 505);
        }
    }
}
