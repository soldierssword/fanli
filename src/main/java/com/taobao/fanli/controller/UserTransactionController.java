package com.taobao.fanli.controller;

import com.taobao.fanli.common.RestResponse;
import com.taobao.fanli.service.LoginService;
import com.taobao.fanli.service.UserTransactionService;
import com.taobao.fanli.vo.Login;
import com.taobao.fanli.vo.UserTransactionListVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
@Api(value="提现接口api", description = "提现接口api")
@RestController
@RequestMapping(value = "/userTransaction")
public class UserTransactionController {

    private static final Logger logger = LoggerFactory.getLogger(UserTransactionController.class);

    @Resource
    private LoginService loginService;
    @Resource
    private UserTransactionService userTransactionService;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation(value="提现列表", notes="提现列表", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "state", value = "提现状态 0：处理 1：完成 2：失败", required = false, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "page", required = true, dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "size", required = false, dataType = "Integer", paramType = "query")
    })
    public RestResponse userTransactionList(
            @RequestParam(value = "state", required = false) Integer state,
            @RequestParam(value = "page") Integer page,
            @RequestParam(value = "size", required = false , defaultValue = "10") Integer size,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Login userByCookie = loginService.getUserByCookie(request);
        if(userByCookie == null){
            return new RestResponse("用户没有登录", 305);
        }
        try {
            UserTransactionListVo list = userTransactionService.list(userByCookie, state, page, size);
            return new RestResponse(list);
        } catch (Exception e) {
            logger.error("提现列表查询失败:", e);
            return new RestResponse("提现列表查询失败", 505);
        }
    }
}
