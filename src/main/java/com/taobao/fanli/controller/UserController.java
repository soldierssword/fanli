package com.taobao.fanli.controller;

import com.taobao.fanli.common.RestResponse;
import com.taobao.fanli.service.LoginService;
import com.taobao.fanli.service.UserService;
import com.taobao.fanli.vo.Login;
import com.taobao.fanli.vo.PersonalInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

/**
 * @author:xiaolei
 * @date:2018/6/11
 */
@RestController
@RequestMapping(value = "/user")
@Api(description="用户接口api")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private LoginService loginService;
    @Resource
    private UserService userService;

    /**
     * 更新用户支付宝号
     * @param alipay
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value="更新用户支付宝号", notes="更新用户支付宝号", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "alipay", value = "支付帐号", required = true, dataType =
                    "String", paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "姓名", required = true, dataType =
                    "String", paramType = "query")
    })
    public RestResponse update(
            @RequestParam(value = "alipay") String alipay,
            @RequestParam(value = "realName") String realName,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Login userByCookie = loginService.getUserByCookie(request);
        if(userByCookie == null){
            return new RestResponse("用户没有登录", 305);
        }
        userService.update(userByCookie, alipay, realName);
        return new RestResponse("success");
    }

    @RequestMapping(value = "/userTransaction", method = RequestMethod.POST)
    @ApiOperation(value="用户提现", notes="用户提现", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "transactionMoney", value = "提现金额", required = true, dataType =
                    "String", paramType = "query")
    })
    public RestResponse userTransaction(
            @RequestParam(value = "transactionMoney") String transactionMoney,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Login userByCookie = loginService.getUserByCookie(request);
        if(userByCookie == null){
            return new RestResponse("用户没有登录", 305);
        }
        BigDecimal receiveMoneyBD = new BigDecimal(userByCookie.getReceiveMoney());
        BigDecimal transactionMoneyPBD = new BigDecimal(transactionMoney);
        if(transactionMoneyPBD.compareTo(receiveMoneyBD) == 1){
            return new RestResponse("可提现金额不足", 505);
        }
        BigDecimal transactionMoneyBD = new BigDecimal(userByCookie.getTransactionMoney());
        String newReceiveMoney = String.valueOf(receiveMoneyBD.subtract(transactionMoneyPBD));
        String newTransactionMoney = String.valueOf(transactionMoneyBD.add(transactionMoneyPBD));
        if(StringUtils.isEmpty(userByCookie.getAlipay())){
            return new RestResponse("用户信息不全, 不能提现", 505);
        }
        userService.userTransaction(userByCookie, transactionMoney, newReceiveMoney, newTransactionMoney);
        return new RestResponse("success");
    }

    /**
     * 个人中心
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/personal", method = RequestMethod.GET)
    @ApiOperation(value="个人中心", notes="个人中心", httpMethod = "GET")
    public RestResponse personal(
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Login userByCookie = loginService.getUserByCookie(request);
        if(userByCookie == null){
            return new RestResponse("用户没有登录", 505);
        }
        PersonalInfo personal = userService.personal(userByCookie);
        return new RestResponse(personal);
    }
}
