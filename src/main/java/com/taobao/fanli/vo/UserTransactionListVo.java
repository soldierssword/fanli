package com.taobao.fanli.vo;

import java.util.List;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
public class UserTransactionListVo {

    private List<UserTransactionInfoVo> userTransactionInfoVos;

    private Integer total;

    public List<UserTransactionInfoVo> getUserTransactionInfoVos() {
        return userTransactionInfoVos;
    }

    public void setUserTransactionInfoVos(List<UserTransactionInfoVo> userTransactionInfoVos) {
        this.userTransactionInfoVos = userTransactionInfoVos;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
