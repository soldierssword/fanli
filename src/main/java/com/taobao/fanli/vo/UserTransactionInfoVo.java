package com.taobao.fanli.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
public class UserTransactionInfoVo {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private Integer id;
    private Integer userId;
    private String alipay;
    private String transactionMoney;
    private Integer state;
    private String createAt;
    private String updateAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }

    public String getTransactionMoney() {
        return transactionMoney;
    }

    public void setTransactionMoney(String transactionMoney) {
        this.transactionMoney = transactionMoney;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = sdf.format(createAt);
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = sdf.format(updateAt);
    }
}
