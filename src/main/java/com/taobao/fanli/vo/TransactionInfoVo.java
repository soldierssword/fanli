package com.taobao.fanli.vo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
public class TransactionInfoVo {

    private static final SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private Integer id;
    private Integer userId;
    private BigDecimal money;
    private Byte type;  // 1:冻结金额 2:可提取金额 3:已提取金额
    private Byte from;  // 1:红包 2:订单 3:用户
    private String createAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Byte getFrom() {
        return from;
    }

    public void setFrom(Byte from) {
        this.from = from;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = sd.format(createAt);
    }
}
