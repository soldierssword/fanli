package com.taobao.fanli.vo;

import java.util.List;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
public class TransactionListVo {

    private List<TransactionInfoVo> transactionInfoVos;

    private Integer total;

    public List<TransactionInfoVo> getTransactionInfoVos() {
        return transactionInfoVos;
    }

    public void setTransactionInfoVos(List<TransactionInfoVo> transactionInfoVos) {
        this.transactionInfoVos = transactionInfoVos;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
