package com.taobao.fanli.vo;

import java.util.List;

/**
 * Created by wanglei on 2018/6/8.
 */
public class UserOrderList {

    private List<UserOrderInfo> userOrderInfos;

    private Integer total;

    public List<UserOrderInfo> getUserOrderInfos(){
        return userOrderInfos;
    }

    public void setUserOrderInfos(List<UserOrderInfo> userOrderInfos){
        this.userOrderInfos = userOrderInfos;
    }

    public Integer getTotal(){
        return total;
    }

    public void setTotal(Integer total){
        this.total = total;
    }
}
