package com.taobao.fanli.dao.mapper;

import com.taobao.fanli.dao.model.UserOrder;
import org.apache.ibatis.annotations.Param;

import javax.annotation.security.PermitAll;
import java.util.Date;
import java.util.List;

public interface UserOrderMapper {

    UserOrder getOne(@Param("id") Integer id);

    void insert(UserOrder userOrder);

    void insertDynamic(UserOrder userOrder);

    void updateDynamic(UserOrder userOrder);

    int delete(Integer id);

    Integer getCountUserOrderList(@Param("userId") Integer userId, @Param("orderState") String
            orderState);

    List<UserOrder> getUserOrderList(@Param("userId") Integer userId, @Param("orderState") String
            orderState, @Param("page") Integer page, @Param("size") Integer size);

    List<Integer> getUserOrderIdList(@Param("userId") Integer id);

    UserOrder selectOrderId(@Param("orderId") Integer orderId);

    UserOrder getOneByOrderNumber(@Param("orderNumber") String orderNumber, @Param("orderId")
            Integer orderId, @Param("state") Integer state);

    Integer getUserRedpackCondition(@Param("userId") Integer userId,@Param("createAt") Date startTime);
}