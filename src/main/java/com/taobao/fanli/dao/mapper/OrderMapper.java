package com.taobao.fanli.dao.mapper;

import com.taobao.fanli.dao.model.Order;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Sun Jun 03 16:30:12 CST 2018
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Sun Jun 03 16:30:12 CST 2018
     */
    int insert(Order record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Sun Jun 03 16:30:12 CST 2018
     */
    int insertSelective(Order record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Sun Jun 03 16:30:12 CST 2018
     */
    Order selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Sun Jun 03 16:30:12 CST 2018
     */
    int updateByPrimaryKeySelective(Order record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *
     * @mbggenerated Sun Jun 03 16:30:12 CST 2018
     */
    int updateByPrimaryKey(Order record);

    List<Order> selectOrderInfo(@Param("orderNumber") String orderNumber);

    String getReceiveMoney(@Param("orderIds") List<Integer> orderIds, @Param("time") Date time);

    String getFrozenMoey(@Param("orderIds") List<Integer> orderIds, @Param("time") Date time);

    Order selectByGoodsIdAndOrderNumber(@Param("goodsId") String goodsId,@Param("orderNumber") String orderNumber);
}