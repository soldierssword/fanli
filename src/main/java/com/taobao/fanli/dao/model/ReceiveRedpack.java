package com.taobao.fanli.dao.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReceiveRedpack {

    private Integer id;
    private BigDecimal money;
    private Date expireTime;
    private String name;
    private Integer status;
    private String expireTimeStr;
    private String description;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
        setExpireTimeStr(expireTime);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getExpireTimeStr() {
        return expireTimeStr;
    }

    public void setExpireTimeStr(Date expireTime) {
        this.expireTimeStr = sdf.format(expireTime);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
