package com.taobao.fanli.dao.model;

import java.math.BigDecimal;
import java.util.Date;

public class Rule {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column rule.id
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column rule.activity_id
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    private Integer activityId;

    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column rule.express_time
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    private Integer expressTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column rule.condition
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    private Integer condition;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column rule.redpack
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    private BigDecimal redpack;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column rule.state
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    private Integer state;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column rule.create_at
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    private Date createAt;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column rule.id
     *
     * @return the value of rule.id
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column rule.id
     *
     * @param id the value for rule.id
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column rule.activity_id
     *
     * @return the value of rule.activity_id
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public Integer getActivityId() {
        return activityId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column rule.activity_id
     *
     * @param activityId the value for rule.activity_id
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column rule.express_time
     *
     * @return the value of rule.express_time
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public Integer getExpressTime() {
        return expressTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column rule.express_time
     *
     * @param expressTime the value for rule.express_time
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public void setExpressTime(Integer expressTime) {
        this.expressTime = expressTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column rule.condition
     *
     * @return the value of rule.condition
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public Integer getCondition() {
        return condition;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column rule.condition
     *
     * @param condition the value for rule.condition
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column rule.redpack
     *
     * @return the value of rule.redpack
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public BigDecimal getRedpack() {
        return redpack;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column rule.redpack
     *
     * @param redpack the value for rule.redpack
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public void setRedpack(BigDecimal redpack) {
        this.redpack = redpack;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column rule.state
     *
     * @return the value of rule.state
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public Integer getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column rule.state
     *
     * @param state the value for rule.state
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column rule.create_at
     *
     * @return the value of rule.create_at
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public Date getCreateAt() {
        return createAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column rule.create_at
     *
     * @param createAt the value for rule.create_at
     *
     * @mbggenerated Fri Jul 13 13:30:44 CST 2018
     */
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}