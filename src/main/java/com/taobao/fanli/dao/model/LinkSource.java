package com.taobao.fanli.dao.model;

import java.util.Date;

public class LinkSource {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link_source.id
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link_source.link_id
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    private Integer linkId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link_source.md5
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    private String md5;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link_source.source
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    private String source;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column link_source.create_at
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    private Date createAt;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link_source.id
     *
     * @return the value of link_source.id
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link_source.id
     *
     * @param id the value for link_source.id
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link_source.link_id
     *
     * @return the value of link_source.link_id
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public Integer getLinkId() {
        return linkId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link_source.link_id
     *
     * @param linkId the value for link_source.link_id
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public void setLinkId(Integer linkId) {
        this.linkId = linkId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link_source.md5
     *
     * @return the value of link_source.md5
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public String getMd5() {
        return md5;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link_source.md5
     *
     * @param md5 the value for link_source.md5
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public void setMd5(String md5) {
        this.md5 = md5;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link_source.source
     *
     * @return the value of link_source.source
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public String getSource() {
        return source;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link_source.source
     *
     * @param source the value for link_source.source
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column link_source.create_at
     *
     * @return the value of link_source.create_at
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public Date getCreateAt() {
        return createAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column link_source.create_at
     *
     * @param createAt the value for link_source.create_at
     *
     * @mbggenerated Sat Sep 08 20:26:13 CST 2018
     */
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}