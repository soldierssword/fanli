package com.taobao.fanli.config;



import com.taobao.fanli.interceptor.MyInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MyWebAppConfigurer 
        extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(getCasUserInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/state/**")
                .excludePathPatterns("/fanli/**")
                .excludePathPatterns("/admin/**")
                .excludePathPatterns("/sso/login");
        super.addInterceptors(registry);
    }

    @Bean
    public MyInterceptor getCasUserInterceptor() {
        return new MyInterceptor();
    }

}