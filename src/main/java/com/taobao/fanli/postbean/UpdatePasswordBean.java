package com.taobao.fanli.postbean;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
public class UpdatePasswordBean {

    private String oldPassword;
    @NotEmpty(message = "新密码不能为空")
    @Length(min = 8, message = "密码最少8个字符")
    private String password;
    @NotEmpty(message = "新密码不能为空")
    @Length(min = 8, message = "密码最少8个字符")
    private String confirmPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
