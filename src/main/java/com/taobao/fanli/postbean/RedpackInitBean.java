package com.taobao.fanli.postbean;

import javax.validation.constraints.NotNull;

public class RedpackInitBean {

    @NotNull(message = "用户还没用注册，请注册信息")
    private Integer userId;

    @NotNull(message = "红包类型不能为空")
    private Integer type;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
