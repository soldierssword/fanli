package com.taobao.fanli.postbean;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
public class UpdateUserTransactionBean {

    private Integer id;
    private Integer userId;
    private Integer state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
