package com.taobao.fanli.postbean;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

/**
 * Created by Tao on 2018/4/19.
 */
public class RegisterBean {

    @NotEmpty(message = "账号不能为空")
    @Length(min = 6, max = 20, message = "账号长度必须在6-20之间")
    @Pattern(regexp = "^[a-z]\\w{4,19}$", message = "账号必须以小写字母开头，由字母数字下划线组成")
    private String account;
    @NotEmpty(message = "密码不能为空")
    @Length(min = 8, message = "密码最少8个字符")
    private String password;
    @NotEmpty(message = "昵称不能为空")
    @Length(min = 2, message = "昵称最少2个字符")
    private String nickName;
    @NotEmpty(message = "姓名不能为空")
    @Pattern(regexp = "[\\u4e00-\\u9fa5]+$", message = "姓名只能是中文")
    private String realName;
    @NotEmpty(message = "支付宝帐号不能为空")
    @Length(min = 6, message = "支付宝帐号不能少于6位")
    private String alipay;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }
}
