package com.taobao.fanli.task;

import com.taobao.fanli.dao.mapper.TaobaoMapper;
import com.taobao.fanli.dao.model.Taobao;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

@Component
public class SchedulerTask {

    @Resource
    private TaobaoMapper taobaoMapper;

    @Scheduled(fixedDelay = 1000*60*3)
    private void process(){
        Taobao taobao = taobaoMapper.queryOne((byte) 1);
        if(taobao == null){
            Taobao first = taobaoMapper.getOne(1, null);
            taobao = new Taobao();
            if(first == null){
                taobao.setId(1);
                taobao.setState((byte) 1);
                taobao.setTitle("Lee女装 2017秋冬新品双口袋时尚休闲牛仔背带短裙 L29975Z02898");
                taobao.setPriceView(new BigDecimal("48900"));
                taobao.setGoodsLink("https://detail.tmall.com/item.htm?id=569885087264");
                taobao.setNativeUrl("");
                taobao.setPicUrl("");
                taobao.setThumbPicUrl("");
                taobao.setSearchContent("€MgJVbahZcMi€");
                taobao.setContent("");
                taobao.setOldTaobaoPassword("€MgJVbahZcMi€");
                taobao.setCreateAt(new Date());
                taobao.setUpdateAt(taobao.getCreateAt());
                taobaoMapper.insertDynamic(taobao);
            }else {
                taobao.setId(first.getId());
                taobao.setState((byte) 1);
                taobaoMapper.updateDynamic(taobao);
            }
        }
    }

    @Scheduled(cron = "0 59 23 * * ?")
    private void deleteTask(){
        taobaoMapper.deleteAll();
    }

}