package com.taobao.fanli.service;

import com.taobao.fanli.dao.mapper.*;
import com.taobao.fanli.dao.model.*;
import com.taobao.fanli.utils.DateUtil;
import com.taobao.fanli.vo.Login;
import com.taobao.fanli.vo.PersonalInfo;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author:xiaolei
 * @date:2018/6/11
 */
@Service
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserOrderMapper userOrderMapper;
    @Resource
    private UserTransactionMapper userTransactionMapper;
    @Resource
    private ActivityMapper activityMapper;
    @Resource
    private RuleMapper ruleMapper;
    @Resource
    private RedpackMapper redpackMapper;
    @Resource
    private TransactionService transactionService;

    public void update(Login userByCookie, String alipay, String realName) {
        User user = new User();
        user.setId(userByCookie.getId());
        user.setAlipay(alipay);
        user.setRealName(realName);
        userMapper.updateDynamic(user);
    }

    public void updateReceiceAndFrozenAndReceiveRedpack(Integer userId){
        //用户红包可领取判断
        List<Activity> activities = activityMapper.selectAll();
        for (Activity activity : activities){
            Integer condition = userOrderMapper.getUserRedpackCondition(userId, activity.getStartTime());
            Rule rule = ruleMapper.getRuleByConditionAndActivityId(activity.getId(), condition);
            if(rule != null){
                Redpack redpack = redpackMapper.checkUserRedpackIsExist(rule.getId(), userId);
                if(redpack != null && redpack.getStatus() == 0){
                    redpack.setStatus(1);
                    redpackMapper.updateByPrimaryKey(redpack);
                }
            }
        }
        updateReceiceAndFrozen(userId);
    }

    //用户红包可领取判断、更新用户可提取金额和冻结金额
    public void updateReceiceAndFrozen(Integer userId){
        String frozenMoney = transactionService.selectByUserIdAndType(userId, TransactionService.FROZENMONEYSTATE);
        String receiveMoney = transactionService.selectByUserIdAndType(userId, TransactionService.RECEIVEMONEYSTATE);
        String transactionMoney = transactionService.selectByUserIdAndType(userId, TransactionService.TRANSACTIONMONEYSTATE);
        User user = new User();
        user.setReceiveMoney(receiveMoney);
        user.setFrozenMoney(frozenMoney);
        user.setTransactionMoney(transactionMoney);
        user.setId(userId);
        user.setUpdateAt(new Date());
        userMapper.updateDynamic(user);
    }

    //个人中心
    public PersonalInfo personal(Login userByCookie) {
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setId(userByCookie.getId());
        personalInfo.setAccount(userByCookie.getAccount());
        personalInfo.setAlipay(userByCookie.getAlipay());
        personalInfo.setNickName(userByCookie.getNickName());
        personalInfo.setRealName(userByCookie.getRealName());
        personalInfo.setFrozenMoney(userByCookie.getFrozenMoney());
        personalInfo.setReceiveMoney(userByCookie.getReceiveMoney());
        personalInfo.setTransactionMoney(userByCookie.getTransactionMoney());
        return personalInfo;
    }

    //交易流水号
    public void userTransaction(Login userByCookie, String transactionMoney, String newReceiveMoney, String newTransactionMoney) {
        UserTransaction userTransaction = new UserTransaction();
        userTransaction.setUserId(userByCookie.getId());
        userTransaction.setAlipay(userByCookie.getAlipay());
        userTransaction.setState(0);
        userTransaction.setTransactionMoney(transactionMoney);
        userTransaction.setCreateAt(new Date());
        userTransaction.setUpdateAt(userTransaction.getCreateAt());
        userTransactionMapper.insertSelective(userTransaction);
        User user = new User();
        user.setId(userByCookie.getId());
        user.setReceiveMoney(newReceiveMoney);
        user.setTransactionMoney(newTransactionMoney);
        user.setUpdateAt(new Date());
        userMapper.updateDynamic(user);

        //减可领取金额
        transactionService.insertOne(userByCookie.getId(), new BigDecimal(transactionMoney).negate(),
                TransactionService.RECEIVEMONEYSTATE, TransactionService.PERSONALFROM, null);
        //加已领取金额
        transactionService.insertOne(userByCookie.getId(), new BigDecimal(transactionMoney),
                TransactionService.TRANSACTIONMONEYSTATE, TransactionService.PERSONALFROM, null);
    }

    public static void main(String[] args) {
        String receiveMoney = "10";
        String transactionMoney = "1";
        BigDecimal a = null;
        String newReceiveMoney = String.valueOf(new BigDecimal(receiveMoney).add(a).subtract(new BigDecimal
                (transactionMoney)));
        System.out.println(newReceiveMoney);
    }

    public Boolean checkUser(Integer userId) {
        User one = userMapper.getOne(userId);
        if(one != null){
            return true;
        }
        return false;
    }

}
