package com.taobao.fanli.service;

import com.taobao.fanli.dao.mapper.UserTransactionMapper;
import com.taobao.fanli.dao.model.UserTransaction;
import com.taobao.fanli.utils.Pager;
import com.taobao.fanli.vo.Login;
import com.taobao.fanli.vo.UserTransactionInfoVo;
import com.taobao.fanli.vo.UserTransactionListVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author:xiaolei
 * @date:2018/8/4
 */
@Service
public class UserTransactionService {
    private static final Logger logger = LoggerFactory.getLogger(UserTransactionService.class);

    @Resource
    private UserTransactionMapper userTransactionMapper;

    public UserTransactionListVo list(Login userByCookie, Integer state, Integer page, Integer size) {
        Integer count = userTransactionMapper.getCount(userByCookie.getId(), state);
        Pager<UserTransaction> pages = new Pager<UserTransaction>(count, page, size);
        List<UserTransaction> userTransactionList = userTransactionMapper.getList(userByCookie.getId(), state, (pages.getPageNumber() - 1) * pages
                        .getLimit(), pages.getLimit());
        if(userTransactionList == null){
            return null;
        }

        List<UserTransactionInfoVo> userTransactionInfoVos = getUserTransactionInfoVos(userTransactionList);

        UserTransactionListVo userTransactionListVo = new UserTransactionListVo();
        userTransactionListVo.setUserTransactionInfoVos(userTransactionInfoVos);
        userTransactionListVo.setTotal(pages.getTotal());
        return userTransactionListVo;

    }

    private List<UserTransactionInfoVo> getUserTransactionInfoVos(List<UserTransaction> userTransactionList) {
        List<UserTransactionInfoVo> userTransactionInfoVos = new ArrayList<>();
        for (UserTransaction userTransaction : userTransactionList){
            UserTransactionInfoVo userTransactionInfoVo = new UserTransactionInfoVo();
            userTransactionInfoVo.setId(userTransaction.getId());
            userTransactionInfoVo.setAlipay(userTransaction.getAlipay());
            userTransactionInfoVo.setCreateAt(userTransaction.getCreateAt());
            userTransactionInfoVo.setState(userTransaction.getState());
            userTransactionInfoVo.setTransactionMoney(userTransaction.getTransactionMoney());
            userTransactionInfoVo.setUpdateAt(userTransaction.getUpdateAt());
            userTransactionInfoVo.setUserId(userTransaction.getUserId());
            userTransactionInfoVos.add(userTransactionInfoVo);
        }
        return userTransactionInfoVos;
    }
}
