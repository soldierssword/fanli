package com.taobao.fanli.service;

import com.taobao.fanli.dao.mapper.OrderMapper;
import com.taobao.fanli.dao.mapper.UserOrderMapper;
import com.taobao.fanli.dao.model.Order;
import com.taobao.fanli.dao.model.UserOrder;
import com.taobao.fanli.utils.Pager;
import com.taobao.fanli.vo.Login;
import com.taobao.fanli.vo.UserOrderInfo;
import com.taobao.fanli.vo.UserOrderList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author:xiaolei
 * @date:2018/6/2
 */
@Service
public class OrderUserService {

    private static final Logger logger = LoggerFactory.getLogger(OrderUserService.class);

    @Resource
    private UserOrderMapper userOrderMapper;
    @Resource
    private OrderMapper orderMapper;


    public UserOrderList getUserOrderList(Login userByCookie,String orderState, Integer page,
                                          Integer size) {
        Integer count = userOrderMapper.getCountUserOrderList(userByCookie.getId(), orderState);
        Pager<UserOrder> pages = new Pager<UserOrder>(count, page, size);
        List<UserOrder> userOrders = userOrderMapper.getUserOrderList(userByCookie.getId(),
                orderState, (pages.getPageNumber() - 1) * pages.getLimit(), pages.getLimit());
        if(userOrders == null){
            return null;
        }
        List<UserOrderInfo> userOrderInfoList = getUserOrderInfoList(userOrders);
        UserOrderList userOrderList = new UserOrderList();
        userOrderList.setUserOrderInfos(userOrderInfoList);
        userOrderList.setTotal(pages.getTotal());
        return userOrderList;
    }

    private List<UserOrderInfo> getUserOrderInfoList(List<UserOrder> userOrders){
        List<UserOrderInfo> list = new ArrayList<>();
        for (UserOrder userOrder : userOrders){
            Integer orderId = userOrder.getOrderId();
            if(orderId != null){
                UserOrderInfo userOrderInfo = new UserOrderInfo();
                Order order = orderMapper.selectByPrimaryKey(orderId);
                userOrderInfo.setOrderId(orderId);
                userOrderInfo.setGoodsInfo(order.getGoodsInfo());
                userOrderInfo.setMoney(order.getMoney());

                userOrderInfo.setOrderNumber(order.getOrderNumber());
                userOrderInfo.setOrderState(order.getOrderState());
                userOrderInfo.setGoodsUrl(order.getGoodsUrl());
                userOrderInfo.setGoodsPic(order.getGoodsPic());
                userOrderInfo.setGoodsTitle(order.getGoodsTitle());
                userOrderInfo.setUserOrderState(userOrder.getState());
                list.add(userOrderInfo);
            }else{
                UserOrderInfo userOrderInfo = new UserOrderInfo();
                if(!StringUtils.isEmpty(userOrder.getOrderNumber())){
                    userOrderInfo.setOrderNumber(userOrder.getOrderNumber());
                }
                userOrderInfo.setUserOrderState(userOrder.getState());
                list.add(userOrderInfo);
            }
        }
        return list;
    }

//    public String query(String orderNumber, Login login) {
//        Order order = orderMapper.selectOrderInfo(orderNumber);
//        if(order != null){
//            UserOrder userOrder = userOrderMapper.selectOrderId(order.getId());
//            if(userOrder != null){
//                throw new RuntimeException("订单已绑定");
//            }
//            userOrder = new UserOrder();
//            userOrder.setOrderId(order.getId());
//            userOrder.setUserId(login.getId());
//            userOrder.setId(null);
//            userOrderMapper.insertDynamic(userOrder);
//            return "success";
//        }
//        return null;
//    }
}
