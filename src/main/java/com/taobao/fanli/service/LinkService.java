package com.taobao.fanli.service;

import com.taobao.fanli.dao.mapper.LinkMapper;
import com.taobao.fanli.dao.mapper.LinkSourceMapper;
import com.taobao.fanli.dao.model.Link;
import com.taobao.fanli.dao.model.LinkSource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LinkService {

    @Resource
    private LinkMapper linkMapper;
    @Resource
    private LinkSourceMapper linkSourceMapper;

    public Link selectByPrimaryKey(Integer id){
        return linkMapper.selectByPrimaryKey(id);
    }

    public int insertSelective(Link record){
        return linkMapper.insertSelective(record);
    }

    public int updateByPrimaryKeySelective(Link record){
        return linkMapper.updateByPrimaryKeySelective(record);
    }

    public int deleteByPrimaryKey(Integer id){
        return linkMapper.deleteByPrimaryKey(id);
    }

    public int updateBySourceId(Integer id, Integer sourceId){
        LinkSource linkSource = linkSourceMapper.selectByPrimaryKey(sourceId);
        if (null == linkSource) return -1;
        Link link = new Link();
        link.setId(id);
        link.setSource(linkSource.getSource());
        return linkMapper.updateByPrimaryKeySelective(link);
    }

}
