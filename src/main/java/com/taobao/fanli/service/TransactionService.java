package com.taobao.fanli.service;

import com.taobao.fanli.dao.mapper.TransactionMapper;
import com.taobao.fanli.dao.model.Transaction;
import com.taobao.fanli.dao.model.UserOrder;
import com.taobao.fanli.utils.Pager;
import com.taobao.fanli.vo.TransactionInfoVo;
import com.taobao.fanli.vo.TransactionListVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);

    public static final byte FROZENMONEYSTATE = 1;  //冻结金额
    public static final byte RECEIVEMONEYSTATE = 2;  //可提取金额
    public static final byte TRANSACTIONMONEYSTATE = 3;   //交易金额

    public static final byte REDPACKFROM = 1;  //红包来源
    public static final byte ORDERFROM = 2;     //订单来源
    public static final byte PERSONALFROM = 3;      //个人来源

    @Resource
    private TransactionMapper transactionMapper;

    //插入
    public void insertOne(Integer userId, BigDecimal money, byte type, byte from, Integer orderId) {
        logger.info("插入流水账， type:" + type +",user_id:" + userId + ",money:" + money);
        Transaction transaction = new Transaction();
        transaction.setUserId(userId);
        transaction.setMoney(money);
        transaction.setType(type);
        transaction.setFrom(from);
        if(orderId != null){
            transaction.setOrderId(orderId);
        }
        transaction.setCreateAt(new Date());
        transactionMapper.insert(transaction);
    }

    public Transaction selectByUserIdAndTypeAndFromAndOrderId(Integer userId, byte type, byte from, Integer orderId) {
        return transactionMapper.selectByUserIdAndTypeAndFromAndOrderId(userId, type, from, orderId);
    }

    public String selectByUserIdAndType(Integer userId, Byte type) {
        return transactionMapper.selectByUserIdAndType(userId, type);
    }

    //列表
    public TransactionListVo list(Integer userId, Byte type, Byte from, Integer page, Integer size) {

        Integer count = transactionMapper.selectCountByUserId(userId, type, from);
        Pager<UserOrder> pages = new Pager<UserOrder>(count, page, size);
        List<Transaction> transactions = transactionMapper.getTransactionList(userId,  type, from, (pages.getPageNumber() - 1) * pages.getLimit(),
                pages.getLimit());
        if(transactions == null){
            return null;
        }

        List<TransactionInfoVo> transactionInfoVos = getTransactionInfoVos(transactions);

        TransactionListVo transactionListVo = new TransactionListVo();
        transactionListVo.setTransactionInfoVos(transactionInfoVos);
        transactionListVo.setTotal(pages.getTotal());
        return transactionListVo;


    }

    private List<TransactionInfoVo> getTransactionInfoVos(List<Transaction> transactions) {
        List<TransactionInfoVo> transactionInfoVos = new ArrayList<>();
        for (Transaction transaction : transactions){
            TransactionInfoVo transactionInfoVo = new TransactionInfoVo();
            transactionInfoVo.setId(transaction.getId());
            transactionInfoVo.setUserId(transaction.getUserId());
            transactionInfoVo.setMoney(transaction.getMoney());
            transactionInfoVo.setType(transaction.getType());
            transactionInfoVo.setFrom(transaction.getFrom());
            transactionInfoVo.setCreateAt(transaction.getCreateAt());
            transactionInfoVos.add(transactionInfoVo);
        }
        return transactionInfoVos;
    }
}
