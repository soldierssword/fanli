package com.taobao.fanli.service;

import com.taobao.fanli.dao.mapper.TransactionMapper;
import com.taobao.fanli.dao.mapper.UserTransactionMapper;
import com.taobao.fanli.dao.model.Transaction;
import com.taobao.fanli.dao.model.UserTransaction;
import com.taobao.fanli.postbean.UpdateUserTransactionBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author:xiaolei
 * @date:2018/6/17
 */
@Service
public class AdminService {

    private static final Logger logger = LoggerFactory.getLogger(AdminService.class);

    @Resource
    private UserTransactionMapper userTransactionMapper;
    @Resource
    private TransactionService transactionService;
    @Resource
    private UserService userService;

    public void updateUserTransaction(UpdateUserTransactionBean updateUserTransactionBean) throws RuntimeException{
        UserTransaction userTransaction = userTransactionMapper.selectByIdAndUserId(updateUserTransactionBean.getId(), updateUserTransactionBean.getUserId());
        if(userTransaction == null){
            throw new RuntimeException("没有此流水号");
        }
        Integer state = updateUserTransactionBean.getState();
        if(state == 2){
            String transactionMoney = userTransaction.getTransactionMoney();
            //减已领取的金额
            transactionService.insertOne(updateUserTransactionBean.getUserId(), new BigDecimal(transactionMoney).negate(), TransactionService
                    .TRANSACTIONMONEYSTATE, TransactionService.PERSONALFROM, null);
            //加可领取的金额
            transactionService.insertOne(updateUserTransactionBean.getUserId(), new BigDecimal(transactionMoney), TransactionService
                    .RECEIVEMONEYSTATE, TransactionService.PERSONALFROM, null);
            //更新用户金额显示
            userService.updateReceiceAndFrozen(updateUserTransactionBean.getUserId());
        }
        userTransaction.setState(state);
        userTransaction.setUpdateAt(new Date());
        userTransactionMapper.updateByPrimaryKeySelective(userTransaction);

    }
}
