package com.taobao.fanli.service;

import com.taobao.fanli.dao.mapper.LinkSourceMapper;
import com.taobao.fanli.dao.model.LinkSource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LinkSourceService {

    @Resource
    private LinkSourceMapper linkSourceMapper;

    public LinkSource selectByPrimaryKey(Integer id){
        return linkSourceMapper.selectByPrimaryKey(id);
    }

    public int insertSelective(LinkSource record){
        return linkSourceMapper.insertSelective(record);
    }

    public int updateByPrimaryKeySelective(LinkSource record){
        return linkSourceMapper.updateByPrimaryKeySelective(record);
    }

    public int deleteByPrimaryKey(Integer id){
        return linkSourceMapper.deleteByPrimaryKey(id);
    }

    public List<LinkSource> selectByLinkId(Integer linkId){
        return linkSourceMapper.selectByLinkId(linkId);
    }
}
